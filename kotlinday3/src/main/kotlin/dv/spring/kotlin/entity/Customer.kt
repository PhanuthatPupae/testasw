package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class Customer(
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = UserStatus.PENDING,
        var isDeleted: Boolean = false
) : User {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
    var billingAddress: Address? = null
    @ManyToOne
    var defaultAddress: Address? = null
}
