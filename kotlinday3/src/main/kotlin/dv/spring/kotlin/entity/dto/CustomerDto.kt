package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.UserStatus

data class CustomerDto(
        var name: String? = null,
        var email: String? = null,
        var userStatus: UserStatus? = UserStatus.PENDING,
        var shippingAddress: List<Address> = mutableListOf<Address>(),
        var billingAddress: Address? = null,
        var defaultAddress: Address? = null
        )