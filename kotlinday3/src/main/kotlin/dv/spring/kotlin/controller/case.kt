//package dv.spring.kotlin.controller
//
//import dv.spring.kotlin.entity.Person
//import dv.spring.kotlin.entity.product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class CaseController {
//    @GetMapping("/getAppName")
//    fun getHelloWorld(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun product(): ResponseEntity<Any> {
//        val Phon = product("iPhone", "A new telephone", 28000, 5)
//        return ResponseEntity.ok(Phon)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getPathParam(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val Phon = product("iPhone", "A new telephone", 28000, 5)
//
//        if(name == Phon.name){
//            return ResponseEntity.ok(Phon)
//        }else{
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//    @PostMapping("/setZeroQuantity")
//    fun resetQuantity(@RequestBody Phon: product): ResponseEntity<Any>{
//        Phon.quantity = 0
//        return ResponseEntity.ok(Phon)
//    }
//
//    @PostMapping("/totalPrice")
//    fun totalPrice(@RequestBody Phon: List<product>): ResponseEntity<Any>{
//        var totalprice = 0
//
//       for( product in Phon ){
//           totalprice = totalprice + product.price
//       }
//        return ResponseEntity.ok(totalprice)
//    }
//
//    @PostMapping("/avaliableProduct")
//    fun avaliableProduct(@RequestBody Phon: Array<product>): ResponseEntity<Any>{
//
//        var List = mutableListOf<product>()
//
//        for(product in Phon){
//            if(product.quantity != 0){
//                List.add(product)
//            }
//        }
//        return ResponseEntity.ok(List)
//    }
//}